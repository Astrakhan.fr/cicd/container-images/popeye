FROM golang

ARG POPEYE_VERSION

RUN git clone https://github.com/derailed/popeye
WORKDIR /go/popeye
RUN git fetch --all --tags
RUN git checkout tags/$POPEYE_VERSION
RUN go install

RUN popeye version